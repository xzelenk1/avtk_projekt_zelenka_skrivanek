﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemperatureMeasuring.aspx.cs" Inherits="Project.TemperatureMeasuring" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" href="Content/bootstrap.min.css" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <link rel="stylesheet" href="StyleSheet.css">

    <title>Regulace teploty v místnosti pomocí teplotního čidla</title>
</head>
<body>

<div class="container">
    <form class="form-group" runat="server">
        <h1>
            <label>Současná teplota: </label>
            <output name="teplota" for="">21°C</output>
        </h1>
        <br>
        <div class="table-responsive" >
        <table class="table table-bordered table-dark" >
            <tbody>
                <tr>
                    <td><asp:Label CssClass="label-text" ID="label1" runat="server" Text="Horní hranice: "></asp:Label></td>
                    <td><asp:TextBox TextMode="Number" ID="TextBox_horni" runat="server" placeholder="24"></asp:TextBox></td>
                    <td><asp:Button CssClass="button" ID="button_horni" runat="server" Text="Nastavit" OnClick="button_horni_Click"/> </td>
                </tr>
                <tr>
                    <td><asp:Label CssClass="label-text" ID="label2" runat="server" Text="Dolní hranice: "></asp:Label></td>
                    <td><asp:TextBox TextMode="Number" ID="TextBox_dolni" runat="server"></asp:TextBox></td>
                    <td><asp:Button CssClass="button" ID="button_dolni" runat="server" Text="Nastavit" OnClick="button_dolni_Click" /></td>
                </tr>
                 <tr>
                    <td><asp:Label CssClass="label-text" ID="label3" runat="server" Text="Roční období: "></asp:Label></td>
                    <td><asp:DropDownList CssClass="select" ID="DropDownList_obdobi" runat="server" name="select_obdobi">
                        <asp:ListItem Text="Jaro" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Leto" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Podzim" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Zima" Value="4"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td><asp:Button CssClass="button" ID="button_obdobi" runat="server" Text="Nastavit" OnClick="button_obdobi_Click"/></td>
                </tr>
                 <tr>
                    <td><asp:Label CssClass="label-text" ID="label_interval" runat="server" Text="Interval měření:"></asp:Label></td>
                    <td><asp:TextBox TextMode="Number" ID="input_interval" runat="server" placeholder="2" name="input_interval"></asp:TextBox></td>
                    <td><asp:Button CssClass="button" ID="button_interval" runat="server" Text="Nastavit" OnClick="button_interval_Click"/></td>

                </tr>
           </tbody>
        </table>
        <table class="table table-bordered table-dark"">
            <tbody>
                <tr>
                    <td><asp:Label CssClass="label-text" runat="server" >Přímotop: <input type="checkbox" data-toggle="toggle" data-onstyle="primary" data-offstyle="danger" id="CheckboxPrimotop"></asp:Label></td>
                    <td><asp:Label  CssClass="label-text" runat="server" >Větrák: <input type="checkbox" data-toggle="toggle" data-onstyle="primary" data-offstyle="danger" id="CheckboxVetrak"></asp:Label></td>                    
                </tr>

            </tbody>
        </table>
        </div>

        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
       
    </form>
    <br>
    <div class="card">
    <asp:Chart ID="TemperaturesChart" runat="server" Height="400" Width="1200" > 
        <Titles><asp:Title Text="Teploty" Font="Calibri, 18pt, style=Bold"></asp:Title></Titles>
        <Series>
            <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Line" MarkerStyle="Diamond" MarkerSize="16" BorderWidth="3" Color="Blue"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
                <AxisX Title="Čas">
                </AxisX>
                <AxisY Title="Teploty">
                </AxisY>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

    </div>
</div>
</body>
</html>
