﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Forecast.Model {
    public class Mereni {
        [Key]
        public int Id { get; set; }
        public int Obdobi { get; set; }
        public DateTime Cas { get; set; }
        public int Hodnota { get; set; }
    }
}
