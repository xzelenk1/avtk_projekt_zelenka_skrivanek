﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Forecast.Model {
    public class MereniContext : DbContext {

        public MereniContext() { }

        public MereniContext(DbContextOptions<MereniContext> options)
            : base(options) {
        }

        public DbSet<Mereni> Namereno { get; set; }
        public DbSet<Vstupy> Vstupies { get; set; }
    }
}
