﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Forecast.Model {
    public class Vstupy {
        [Key]
        public int Id { get; set; }
        public int Horni_hranice { get; set; }
        public int Dolni_hranice { get; set; }
        public int Obdobi { get; set; }
        public int Interval { get; set; }
    }
}
