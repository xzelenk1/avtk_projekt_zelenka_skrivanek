﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Windows.Forms;
using RestSharp;
using WebApi.Models;

namespace Project {
    public static class GlobalVariables {
        public static bool nacti = true;
    }
    public partial class TemperatureMeasuring : System.Web.UI.Page {

        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True");
        int horni = 0;
        int dolni = 20;
        List<Mereni> namereno;

        protected void Page_Load(object sender, EventArgs e) {
            if (con.State == ConnectionState.Open) {
                con.Close();
            }
            con.Open();

            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;

            //cmd.CommandText = "DELETE FROM Mereni";
            //cmd.ExecuteNonQuery();

            //cmd.CommandText = "DBCC CHECKIDENT(Mereni, RESEED, 0)";
            //cmd.ExecuteNonQuery();

            /*cmd.CommandText = "INSERT INTO Mereni values('1', '06/24/1997 12:09', '23')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO Mereni values('1', '06/25/1997 12:09', '24')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO Mereni values('1', '06/28/1997 12:09', '52')";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "INSERT INTO Mereni values('1', '06/29/1997 12:09', '32')";
            cmd.ExecuteNonQuery();*/
            /*for (int i = 0; i < 5; i++) {
                cmd.ExecuteNonQuery();
            }
            */

            if (GlobalVariables.nacti) {
                reload();
                GlobalVariables.nacti = false;
            }

            var restClient = new RestClient(new Uri("https://localhost:44346/"));
            var request = new RestRequest("api/Mereni", Method.GET);
            var mereni = restClient.Execute<List<Mereni>>(request);
            namereno = mereni.Data;

            Series ser = TemperaturesChart.Series["Series1"];
            ser.Points.AddXY(namereno[0].Cas, namereno[0].Hodnota);
            ser.Points.AddXY(namereno[1].Cas, namereno[1].Hodnota);
            ser.Points.AddXY(namereno[2].Cas, namereno[2].Hodnota);
            /*cmd.CommandText = "SELECT cas, hodnota FROM Mereni";

            SqlDataReader rdr = cmd.ExecuteReader();
            TemperaturesChart.Series["Series1"].XValueMember = "cas";
            TemperaturesChart.Series["Series1"].YValueMembers = "hodnota";
            TemperaturesChart.DataSource = rdr;
            TemperaturesChart.DataBind();*/
            TemperaturesChart.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);

//            rdr.Close();


            /*using (SqlDataReader reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    series.Points.AddXY(reader["datum"].ToString(), reader["hodnota"]);
                }
            }*/

            //DisplayData();

        }

        private void DisplayData() {
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM Mereni";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            
        }

        private void reload() {
            SqlCommand comd = con.CreateCommand();
            comd.CommandText = "SELECT * FROM Inputs WHERE id = '1'";
            using (SqlDataReader reader = comd.ExecuteReader()) {
                reader.Read();
                TextBox_horni.Text = reader["horni_hranice"].ToString();
                TextBox_dolni.Text = reader["dolni_hranice"].ToString();
                input_interval.Text = reader["interval"].ToString();
                DropDownList_obdobi.SelectedValue = reader["obdobi"].ToString();
                reader.Close();
            }
        }

        protected void button_horni_Click(object sender, EventArgs e) {
            int.TryParse(TextBox_horni.Text, out horni);
            int.TryParse(TextBox_dolni.Text, out dolni);
            if (horni > dolni) {
                SqlCommand comd = con.CreateCommand();
                comd.CommandType = CommandType.Text;
                comd.CommandText = "UPDATE Inputs SET horni_hranice = " + TextBox_horni.Text + " WHERE id = '1'";
                comd.ExecuteNonQuery();
            } else {
                MessageBox.Show("Hranice horní teploty nemůže být nižší než dolní hranice.");
            }
        }

        protected void button_dolni_Click(object sender, EventArgs e) {
            int.TryParse(TextBox_horni.Text, out horni);
            int.TryParse(TextBox_dolni.Text, out dolni);
            if (horni > dolni) {
                SqlCommand comd = con.CreateCommand();
                comd.CommandType = CommandType.Text;
                comd.CommandText = "UPDATE Inputs SET dolni_hranice = " + TextBox_dolni.Text + " WHERE id = '1'";
                comd.ExecuteNonQuery();
            } else {
                MessageBox.Show("Hranice horní teploty nemůže být nižší než dolní hranice.");
            }
        }

        protected void button_interval_Click(object sender, EventArgs e) {
            if (int.Parse(input_interval.Text) < 1) {
                MessageBox.Show("Hodnota intervalu nemůže být nižší než 1");
            } else {
                SqlCommand comd = con.CreateCommand();
                comd.CommandType = CommandType.Text;
                comd.CommandText = "UPDATE Inputs SET interval = " + input_interval.Text + " WHERE id = '1'";
                comd.ExecuteNonQuery();
            }
        }

        protected void button_obdobi_Click(object sender, EventArgs e) {
            SqlCommand comd = con.CreateCommand();
            comd.CommandType = CommandType.Text;
            comd.CommandText = "UPDATE Inputs SET obdobi = '" + DropDownList_obdobi.SelectedValue + "' WHERE id = '1'";
            comd.ExecuteNonQuery();
        }
    }
}