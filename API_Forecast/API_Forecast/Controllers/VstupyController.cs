﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Forecast.Model;

namespace API_Forecast.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VstupyController : ControllerBase
    {
        private readonly MereniContext _context;

        public VstupyController(MereniContext context)
        {
            _context = context;
        }

        // GET: api/Vstupy
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vstupy>>> GetVstupies()
        {
            return await _context.Vstupies.ToListAsync();
        }

        // GET: api/Vstupy/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Vstupy>> GetVstupy(int id)
        {
            var vstupy = await _context.Vstupies.FindAsync(id);

            if (vstupy == null)
            {
                return NotFound();
            }

            return vstupy;
        }

        // PUT: api/Vstupy/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVstupy(int id, Vstupy vstupy)
        {
            if (id != vstupy.Id)
            {
                return BadRequest();
            }

            _context.Entry(vstupy).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VstupyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Vstupy
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Vstupy>> PostVstupy(Vstupy vstupy)
        {
            _context.Vstupies.Add(vstupy);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVstupy", new { id = vstupy.Id }, vstupy);
        }

        // DELETE: api/Vstupy/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Vstupy>> DeleteVstupy(int id)
        {
            var vstupy = await _context.Vstupies.FindAsync(id);
            if (vstupy == null)
            {
                return NotFound();
            }

            _context.Vstupies.Remove(vstupy);
            await _context.SaveChangesAsync();

            return vstupy;
        }

        private bool VstupyExists(int id)
        {
            return _context.Vstupies.Any(e => e.Id == id);
        }
    }
}
