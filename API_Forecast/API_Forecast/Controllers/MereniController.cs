﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Forecast.Model;

namespace API_Forecast.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MereniController : ControllerBase
    {
        private readonly MereniContext _context;

        public MereniController(MereniContext context)
        {
            _context = context;
        }

        // GET: api/Mereni
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Mereni>>> GetNamereno()
        {
            return await _context.Namereno.ToListAsync();
        }

        // GET: api/Mereni/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Mereni>> GetMereni(int id)
        {
            var mereni = await _context.Namereno.FindAsync(id);

            if (mereni == null)
            {
                return NotFound();
            }

            return mereni;
        }

        // PUT: api/Mereni/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMereni(int id, Mereni mereni)
        {
            if (id != mereni.Id)
            {
                return BadRequest();
            }

            _context.Entry(mereni).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MereniExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Mereni
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Mereni>> PostMereni(Mereni mereni)
        {
            _context.Namereno.Add(mereni);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMereni), new { id = mereni.Id }, mereni);
        }

        // DELETE: api/Mereni/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Mereni>> DeleteMereni(int id)
        {
            var mereni = await _context.Namereno.FindAsync(id);
            if (mereni == null)
            {
                return NotFound();
            }

            _context.Namereno.Remove(mereni);
            await _context.SaveChangesAsync();

            return mereni;
        }

        private bool MereniExists(int id)
        {
            return _context.Namereno.Any(e => e.Id == id);
        }
    }
}
