﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Windows.Forms;
using RestSharp;
using ProjectAPI.Models;
using Label = System.Web.UI.WebControls.Label;
using System.Web.UI.HtmlControls;
using MoreLinq;

namespace Project {
    public static class GlobalVariables {
        public static bool nacti = true;
    }
    public partial class TemperatureMeasuring : System.Web.UI.Page {

        int horni = 0;
        int dolni = 20;
        List<Mereni> namereno;

        protected void Page_Load(object sender, EventArgs e) {

            //setCheckBoxesStyle();
            
            var restClient = new RestClient(new Uri("https://localhost:44327/"));
            var request = new RestRequest("api/Mereni", Method.GET);
            var mereni = restClient.Execute<List<Mereni>>(request);
            namereno = mereni.Data;
            if (namereno.Count > 10) {
                namereno = namereno.TakeLast(10).ToList();
            }

            if (GlobalVariables.nacti) {
                reload();
                GlobalVariables.nacti = false;
            }

            Label teplota_label = this.FindControl("teplota_label") as Label;
            teplota_label.Text = namereno[namereno.Count - 1].Hodnota.ToString() + "°C";

            TemperaturesChart.Series["Series1"].XValueMember = "Cas";
            TemperaturesChart.Series["Series1"].YValueMembers = "Hodnota";
            TemperaturesChart.DataSource = namereno;
            TemperaturesChart.DataBind();
            TemperaturesChart.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 12, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new System.Drawing.Font("Calibri", 12, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);
            TemperaturesChart.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Calibri", 14, System.Drawing.FontStyle.Bold);

        }

        //TODO nefunguje pak klikani
        private void setCheckBoxesStyle() {
            CheckboxVetrak.InputAttributes.Add("data-toggle", "toggle");
            CheckboxVetrak.InputAttributes.Add("data-onstyle", "primary");
            CheckboxVetrak.InputAttributes.Add("data-offstyle", "danger");
            CheckboxPrimotop.InputAttributes.Add("data-toggle", "toggle");
            CheckboxPrimotop.InputAttributes.Add("data-onstyle", "primary");
            CheckboxPrimotop.InputAttributes.Add("data-offstyle", "danger");
        }

        private void reload() {
            //nacteni vstupu z databaze
            var restClient = new RestClient(new Uri("https://localhost:44327/"));
            var request = new RestRequest("api/Vstupy/1", Method.GET);
            var vstup = restClient.Execute<Vstupy>(request);
            TextBox_horni.Text = vstup.Data.Horni_hranice.ToString();
            TextBox_dolni.Text = vstup.Data.Dolni_hranice.ToString();
            input_interval.Text = vstup.Data.Interval.ToString();
            DropDownList_obdobi.SelectedValue = vstup.Data.Obdobi.ToString();
            
            //nacteni nastaveni posledniho spusteni vetraku a primotopu
            request = new RestRequest("api/Mereni", Method.GET);
            var mereni = restClient.Execute<List<Mereni>>(request);
            namereno = mereni.Data;
            CheckboxVetrak.Checked = namereno[namereno.Count - 1].Vetrak;
            CheckboxPrimotop.Checked = namereno[namereno.Count - 1].Primotop;

            //nastaveni vstupu na posledni hodnoty mereni
            request = new RestRequest("api/Vstupy/1", Method.PUT);
            vstup.Data.Primotop = namereno[namereno.Count - 1].Primotop;
            vstup.Data.Vetrak = namereno[namereno.Count - 1].Vetrak;
            request.AddJsonBody(vstup.Data);
            restClient.Execute(request);
        }

        protected void button_horni_Click(object sender, EventArgs e) {
            int.TryParse(TextBox_horni.Text, out horni);
            int.TryParse(TextBox_dolni.Text, out dolni);
            if (horni > dolni) {
                var restClient = new RestClient(new Uri("https://localhost:44327/"));
                var request = new RestRequest("api/Vstupy/1", Method.GET);
                var vstupy = restClient.Execute<Vstupy>(request);
                var vstup = vstupy.Data;
                vstup.Horni_hranice = horni;
                request = new RestRequest("api/Vstupy/1", Method.PUT);
                request.AddJsonBody(vstup);
                restClient.Execute(request);
            } else {
                MessageBox.Show("Hranice horní teploty nemůže být nižší než dolní hranice.");
                reload();
            }
        }

        protected void button_dolni_Click(object sender, EventArgs e) {
            int.TryParse(TextBox_horni.Text, out horni);
            int.TryParse(TextBox_dolni.Text, out dolni);
            if (horni > dolni) {
                var restClient = new RestClient(new Uri("https://localhost:44327/"));
                var request = new RestRequest("api/Vstupy/1", Method.GET);
                var vstupy = restClient.Execute<Vstupy>(request);
                var vstup = vstupy.Data;
                vstup.Dolni_hranice = dolni;
                request = new RestRequest("api/Vstupy/1", Method.PUT);
                request.AddJsonBody(vstup);
                restClient.Execute(request);
            } else {
                MessageBox.Show("Hranice horní teploty nemůže být nižší než dolní hranice.");
                reload();
            }
        }

        protected void button_interval_Click(object sender, EventArgs e) {
            if (int.Parse(input_interval.Text) < 1) {
                MessageBox.Show("Hodnota intervalu nemůže být nižší než 1");
                reload();
            } else {
                var restClient = new RestClient(new Uri("https://localhost:44327/"));
                var request = new RestRequest("api/Vstupy/1", Method.GET);
                var vstupy = restClient.Execute<Vstupy>(request);
                var vstup = vstupy.Data;
                vstup.Interval = int.Parse(input_interval.Text);
                request = new RestRequest("api/Vstupy/1", Method.PUT);
                request.AddJsonBody(vstup);
                restClient.Execute(request);
            }
        }

        protected void button_obdobi_Click(object sender, EventArgs e) {
            var restClient = new RestClient(new Uri("https://localhost:44327/"));
            var request = new RestRequest("api/Vstupy/1", Method.GET);
            var vstupy = restClient.Execute<Vstupy>(request);
            var vstup = vstupy.Data;
            vstup.Obdobi = int.Parse(DropDownList_obdobi.SelectedValue);
            request = new RestRequest("api/Vstupy/1", Method.PUT);
            request.AddJsonBody(vstup);
            restClient.Execute(request);
        }

        protected void CheckboxPrimotop_CheckedChanged(object sender, EventArgs e) {
            var restClient = new RestClient(new Uri("https://localhost:44327/"));
            var request = new RestRequest("api/Vstupy/1", Method.GET);
            var vstupy = restClient.Execute<Vstupy>(request);
            var vstup = vstupy.Data;
            vstup.Primotop = CheckboxPrimotop.Checked;
            request = new RestRequest("api/Vstupy/1", Method.PUT);
            request.AddJsonBody(vstup);
            restClient.Execute(request);
        }

        protected void CheckboxVetrak_CheckedChanged(object sender, EventArgs e) {
            var restClient = new RestClient(new Uri("https://localhost:44327/"));
            var request = new RestRequest("api/Vstupy/1", Method.GET);
            var vstupy = restClient.Execute<Vstupy>(request);
            var vstup = vstupy.Data;
            vstup.Vetrak = CheckboxVetrak.Checked;
            request = new RestRequest("api/Vstupy/1", Method.PUT);
            request.AddJsonBody(vstup);
            restClient.Execute(request); reload();
        }

        protected void buttonRefresh_Click(object sender, EventArgs e) {
            reload();
        }
    }
}