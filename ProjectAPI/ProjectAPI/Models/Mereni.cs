﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectAPI.Models {
    public class Mereni {
        [Key]
        public int Id { get; set; }
        public int Obdobi { get; set; }
        public DateTime Cas { get; set; }
        public int Hodnota { get; set; }
        public bool Vetrak { get; set; }
        public bool Primotop { get; set; }
    }
}