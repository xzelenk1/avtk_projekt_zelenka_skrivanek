﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectAPI.Models {
    public class MereniContext : DbContext {
        public MereniContext() : base("DatabaseMereni") {
        }

        public DbSet<Mereni> Namereno { get; set; }
        public DbSet<Vstupy> Vstupy { get; set; }
    }
}