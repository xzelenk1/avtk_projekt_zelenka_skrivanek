﻿//------------------------------------------------------------------------------
// <automaticky generován>
//     Tento kód vygeneroval nástroj.
//
//     Změny tohoto souboru mohou způsobit nesprávné chování a budou ztraceny, jestliže
//     kód je znovu vygenerován. 
// </automaticky generován>
//------------------------------------------------------------------------------

namespace Project
{


    public partial class TemperatureMeasuring
    {

        /// <summary>
        /// ovládací prvek form1.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// ovládací prvek teplota_label.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label teplota_label;

        /// <summary>
        /// ovládací prvek label1.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label label1;

        /// <summary>
        /// ovládací prvek TextBox_horni.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox_horni;

        /// <summary>
        /// ovládací prvek button_horni.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button button_horni;

        /// <summary>
        /// ovládací prvek label2.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label label2;

        /// <summary>
        /// ovládací prvek TextBox_dolni.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox_dolni;

        /// <summary>
        /// ovládací prvek button_dolni.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button button_dolni;

        /// <summary>
        /// ovládací prvek label3.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label label3;

        /// <summary>
        /// ovládací prvek DropDownList_obdobi.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DropDownList_obdobi;

        /// <summary>
        /// ovládací prvek button_obdobi.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button button_obdobi;

        /// <summary>
        /// ovládací prvek label_interval.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label label_interval;

        /// <summary>
        /// ovládací prvek input_interval.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox input_interval;

        /// <summary>
        /// ovládací prvek button_interval.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button button_interval;

        /// <summary>
        /// ovládací prvek CheckboxPrimotop.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox CheckboxPrimotop;

        /// <summary>
        /// ovládací prvek CheckboxVetrak.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox CheckboxVetrak;

        /// <summary>
        /// ovládací prvek buttonRefresh.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button buttonRefresh;

        /// <summary>
        /// ovládací prvek GridView1.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridView1;

        /// <summary>
        /// ovládací prvek TemperaturesChart.
        /// </summary>
        /// <remarks>
        /// Automaticky generované pole.
        /// Pro úpravy přesuňte pole deklarace ze souboru návrháře do souboru kódu na pozadí.
        /// </remarks>
        protected global::System.Web.UI.DataVisualization.Charting.Chart TemperaturesChart;
    }
}
