﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProjectAPI.Models;

namespace ProjectAPI.Controllers
{
    public class MereniController : ApiController
    {
        private MereniContext db = new MereniContext();

        // GET: api/Mereni
        public IQueryable<Mereni> GetNamereno()
        {
            return db.Namereno;
        }

        // GET: api/Mereni/5
        [ResponseType(typeof(Mereni))]
        public async Task<IHttpActionResult> GetMereni(int id)
        {
            Mereni mereni = await db.Namereno.FindAsync(id);
            if (mereni == null)
            {
                return NotFound();
            }

            return Ok(mereni);
        }

        // PUT: api/Mereni/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMereni(int id, Mereni mereni)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mereni.Id)
            {
                return BadRequest();
            }

            db.Entry(mereni).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MereniExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Mereni
        [ResponseType(typeof(Mereni))]
        public async Task<IHttpActionResult> PostMereni(Mereni mereni)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Namereno.Add(mereni);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = mereni.Id }, mereni);
        }

        // DELETE: api/Mereni/5
        [ResponseType(typeof(Mereni))]
        public async Task<IHttpActionResult> DeleteMereni(int id)
        {
            Mereni mereni = await db.Namereno.FindAsync(id);
            if (mereni == null)
            {
                return NotFound();
            }

            db.Namereno.Remove(mereni);
            await db.SaveChangesAsync();

            return Ok(mereni);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MereniExists(int id)
        {
            return db.Namereno.Count(e => e.Id == id) > 0;
        }
    }
}