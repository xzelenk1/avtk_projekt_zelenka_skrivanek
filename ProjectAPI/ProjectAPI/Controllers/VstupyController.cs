﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ProjectAPI.Models;

namespace ProjectAPI.Controllers
{
    public class VstupyController : ApiController
    {
        private MereniContext db = new MereniContext();

        // GET: api/Vstupy
        public IQueryable<Vstupy> GetVstupy()
        {
            return db.Vstupy;
        }

        // GET: api/Vstupy/5
        [ResponseType(typeof(Vstupy))]
        public async Task<IHttpActionResult> GetVstupy(int id)
        {
            Vstupy vstupy = await db.Vstupy.FindAsync(id);
            if (vstupy == null)
            {
                return NotFound();
            }

            return Ok(vstupy);
        }

        // PUT: api/Vstupy/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVstupy(int id, Vstupy vstupy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vstupy.Id)
            {
                return BadRequest();
            }

            db.Entry(vstupy).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VstupyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vstupy
        [ResponseType(typeof(Vstupy))]
        public async Task<IHttpActionResult> PostVstupy(Vstupy vstupy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vstupy.Add(vstupy);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = vstupy.Id }, vstupy);
        }

        // DELETE: api/Vstupy/5
        [ResponseType(typeof(Vstupy))]
        public async Task<IHttpActionResult> DeleteVstupy(int id)
        {
            Vstupy vstupy = await db.Vstupy.FindAsync(id);
            if (vstupy == null)
            {
                return NotFound();
            }

            db.Vstupy.Remove(vstupy);
            await db.SaveChangesAsync();

            return Ok(vstupy);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VstupyExists(int id)
        {
            return db.Vstupy.Count(e => e.Id == id) > 0;
        }
    }
}