﻿namespace ProjectAPI.Migrations
{
    using ProjectAPI.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectAPI.Models.MereniContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProjectAPI.Models.MereniContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Namereno.AddOrUpdate(x => x.Id,
                new Mereni() { Id = 1, Obdobi = 1, Cas = new DateTime(2020, 4, 25, 15, 10, 00), Hodnota = 24 },
                new Mereni() { Id = 2, Obdobi = 1, Cas = new DateTime(2020, 4, 26, 15, 10, 00), Hodnota = 25 },
                new Mereni() { Id = 3, Obdobi = 1, Cas = new DateTime(2020, 4, 27, 15, 10, 00), Hodnota = 26 }
                );
            context.Vstupy.AddOrUpdate(x => x.Id,
                new Vstupy() { Id = 1, Dolni_hranice = 18, Horni_hranice = 28, Interval = 300, Obdobi = 1 }
                );
        }
    }
}
