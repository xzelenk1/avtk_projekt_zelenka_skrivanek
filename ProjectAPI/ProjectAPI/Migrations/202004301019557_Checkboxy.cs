﻿namespace ProjectAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Checkboxy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Merenis", "Vetrak", c => c.Boolean(nullable: false));
            AddColumn("dbo.Merenis", "Primotop", c => c.Boolean(nullable: false));
            AddColumn("dbo.Vstupies", "Vetrak", c => c.Boolean(nullable: false));
            AddColumn("dbo.Vstupies", "Primotop", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vstupies", "Primotop");
            DropColumn("dbo.Vstupies", "Vetrak");
            DropColumn("dbo.Merenis", "Primotop");
            DropColumn("dbo.Merenis", "Vetrak");
        }
    }
}
