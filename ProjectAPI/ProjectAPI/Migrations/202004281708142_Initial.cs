﻿namespace ProjectAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Merenis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Obdobi = c.Int(nullable: false),
                        Cas = c.DateTime(nullable: false),
                        Hodnota = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vstupies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Horni_hranice = c.Int(nullable: false),
                        Dolni_hranice = c.Int(nullable: false),
                        Obdobi = c.Int(nullable: false),
                        Interval = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Vstupies");
            DropTable("dbo.Merenis");
        }
    }
}
