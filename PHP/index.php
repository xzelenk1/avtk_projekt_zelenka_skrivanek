<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css">
    <title>Title</title>
    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Graf teplot"
                },
                axisY:{
                    includeZero: false
                },
                data: [{
                    type: "line",
                    indexLabelFontSize: 16,
                    dataPoints: [
                        { x: 0, y: 10 },
                        { x: 2, y: 10 },
                        { x: 4, y: 11 },
                        { x: 6, y: 12 },
                        { x: 8, y: 14 },
                        { x: 10, y: 16 },
                        { x: 12, y: 18 },
                        { x: 14, y: 22 },
                        { x: 16, y: 20 },
                        { x: 18, y: 20 },
                        { x: 20, y: 19 },
                        { x: 22, y: 16 }
                    ]
                }]
            });
            chart.render();

        }
    </script>
</head>
<body>
<div class="content">
    <form class="form-group">
        <h1>
            <label>Současná teplota: </label>
            <output name="teplota" for="">21°C</output>
        </h1>
        <br>
        <div class="form-group">
            <div class="form-row">
                <p>
                    <label class="label-text">Horní hranice: </label>
                    <input type="number" placeholder="24" name="input_horni">
                    <input class="button" type="button" value="Nastavit" name="button_horni">
                </p>
            </div>
            <div class="form-row">
                <p>
                    <label class="label-text">Dolní hranice: </label>
                    <input type="number" placeholder="18" name="input_dolni" id="input_dolni">
                    <input class="button" type="button" value="Nastavit" name="button_dolni">
                </p>
            </div>
            <div class="form-row">
                <p>
                    <label class="label-text">Roční období: </label>
                    <select id="select_obdobi" name="select_obdobi" class="select">
                        <option value="1">Jaro</option>
                        <option value="2">Leto</option>
                        <option value="3">Podzim</option>
                        <option value="4">Zima</option>
                    </select>
                    <input class="button" type="button" value="Nastavit" name="button_obdobi">
                </p>
            </div>
            <div class="form-row">
                <p>
                    <label class="label-text">Interval měření:</label>
                    <input type="number" placeholder="2" name="input_interval">
                    <input class="button" type="button" value="Nastavit" name="button_interval">
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <p>
                    <label class="label-text">Přímotop (Zapnuto/Vypnuto)</label>
                    <label style="color: white">AYAYAYAYAY</label> <!-- Its a fičura -->
                    <label class="label-text">Větrák (Zapnuto/Vypnuto)</label>
                </p>
            </div>
            <div class="form-row">
                <label class="switch">
                    <input type="checkbox" name="switch1" id="switch1">
                    <span class="slider round"></span>
                </label>
                <label class="switch">
                    <input type="checkbox" name="switch2" id="switch2">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
    </form>
    <br><br><br>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</div>
</body>
</html>
